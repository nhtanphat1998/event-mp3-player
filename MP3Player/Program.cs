﻿using NAudio.Wave;

// Load tất cả các file .mp3 lên sau đó chạy
var songs = new string[]
{
    "UngQuaChung-AMEE-8783624.mp3",
    "TinhYeuKhungLong-FAY-6247040.mp3"
};

var mp3Player = new MP3Player(songs);
mp3Player.Play();
Console.ReadLine();


class MP3Player
{
    private string[] songs;
    private int currentSongIndex;

    private AudioFileReader audioFile;
    private WaveOutEvent outputDevice;

    public MP3Player(string[] songList)
    {
        songs = songList;
        currentSongIndex = 0;
        audioFile = new AudioFileReader(songs[currentSongIndex]);
        outputDevice = new WaveOutEvent();
        outputDevice.Init(audioFile);
        outputDevice.PlaybackStopped += HandleWhenFileStopped;
    }

    public void Play()
    {
        outputDevice.Play();
        Console.WriteLine("Music is playing: " + songs[currentSongIndex]);
    }

    private void HandleWhenFileStopped(object sender, StoppedEventArgs e)
    {
        // Tự động kích hoạt khi bài hát kết thúc
        Console.WriteLine("Audio playback finished: " + songs[currentSongIndex]);

        // Chuyển sang bài hát tiếp theo
        currentSongIndex++;
        if (currentSongIndex >= songs.Length)
        {
            currentSongIndex = 0; // Quay lại bài đầu tiên nếu đã hết danh sách
        }

        audioFile.Dispose();
        // Chuyển sang bài hát mới
        audioFile = new AudioFileReader(songs[currentSongIndex]);
        outputDevice.Init(audioFile);
        Play();
    }
}